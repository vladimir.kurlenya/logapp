export TAG=0.0.1
export TF_VAR_logapp_tag=$TAG

AWS_ACCOUNT_ID=$(aws sts get-caller-identity --query "Account" --output text)
AWS_REGION=us-east-1
AWS_ECR_BASE_URL="${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com"

aws ecr create-repository --repository-name=logapp --image-scanning-configuration scanOnPush=true 2> /dev/null || true
aws ecr get-login-password --region ${AWS_REGION} | docker login --username AWS --password-stdin ${AWS_ECR_BASE_URL}

docker build -t ${AWS_ECR_BASE_URL}/logapp:${TAG} -f ./docker/Dockerfile ./app
docker push ${AWS_ECR_BASE_URL}/logapp:${TAG}

cd terraform
terraform init
terraform plan -out=tf.plan -input=false
HELM_DEBUG=1 terraform apply -auto-approve -input=false tf.plan