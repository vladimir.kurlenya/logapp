locals {
  db_tags = {
    Application = "logapp"
  }
}

resource "random_password" "logapp_db_password" {
  length  = 32
  special = false // PSQL CLI doesn't like special characters
}

module "rds_cluster_aurora" {
  enabled             = true
  source              = "./modules/rds"
  engine              = "aurora-postgresql"
  engine_version      = "13.3"
  cluster_family      = "aurora-postgresql13"
  cluster_size        = "1"
  name                = "logapp"
  admin_user          = "postgres"
  admin_password      = random_password.logapp_db_password.result
  db_name             = "logapp"
  tags                = local.db_tags
  instance_type       = "db.t4g.medium"
  vpc_id              = module.vpc.vpc_id
  availability_zones  = var.availability_zones
  deletion_protection = false

  security_groups = [
    aws_security_group.eks_postgres.id,
    aws_security_group.all_traffic_outbound.id,
  ]

  subnets             = module.vpc.private_subnets_ids
  publicly_accessible = false
  storage_encrypted   = true
  kms_key_id          = aws_kms_key.logapp_db_kms_key.arn
  apply_immediately   = true
  iam_auth_enabled    = true
}

resource "aws_kms_key" "logapp_db_kms_key" {
  description         = "logapp rds aurora encryption key"
  enable_key_rotation = false

  tags = local.db_tags
}

resource "aws_kms_alias" "logapp_db_kms_key" {
  name          = "alias/logapp-rds-aurora-postgres"
  target_key_id = aws_kms_key.logapp_db_kms_key.key_id
}
