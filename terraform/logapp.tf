resource "helm_release" "logapp" {
  name             = "logapp"
  namespace        = "logapp"
  create_namespace = true
  repository       = "../helm"
  chart            = "logapp"
  version          = "6.0.1"

  values = [
    file("../helm/values/logapp/common.yaml")
  ]

  set {
    name  = "image.repository"
    value = data.aws_ecr_repository.logapp.repository_url
  }

  set {
    name  = "image.tag"
    value = var.logapp_tag
  }

  set {
    name  = "secretEnvVars.DATABASE_URL"
    value = "postgresql://${module.rds_cluster_aurora.user}:${module.rds_cluster_aurora.password}@${module.rds_cluster_aurora.endpoint}:${module.rds_cluster_aurora.port}/${module.rds_cluster_aurora.name}"
  }

  set {
    name  = "serviceAccount.annotations.alb\\.ingress\\.kubernetes\\.io/security-groups"
    value = aws_security_group.alb_public.id
  }

  depends_on = [
    helm_release.aws-load-balancer-controller
  ]
}
