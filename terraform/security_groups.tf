resource "aws_security_group" "eks_postgres" {
  name        = "eks-postgres"
  description = "tf-sg-eks-postgres"
  vpc_id      = module.vpc.vpc_id
}

resource "aws_security_group_rule" "eks_postgres" {
  security_group_id = aws_security_group.eks_postgres.id
  from_port         = 5432
  to_port           = 5432
  protocol          = "tcp"
  cidr_blocks       = var.eks_workers_subnets
  type              = "ingress"
}

resource "aws_security_group" "all_traffic_outbound" {
  name        = "all-traffic-outbound"
  description = "tf-all-traffic-outbound"
  vpc_id      = module.vpc.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "alb_private" {
  name   = "alb-private"
  vpc_id = module.vpc.vpc_id
}

resource "aws_security_group_rule" "ingress_http_alb_private" {
  security_group_id = aws_security_group.alb_private.id
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = [var.vpc_cidr]
  type              = "ingress"
}

resource "aws_security_group_rule" "ingress_https_alb_private" {
  security_group_id = aws_security_group.alb_private.id
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = [var.vpc_cidr]
  type              = "ingress"
}

resource "aws_security_group" "alb_public" {
  name   = "alb-public"
  vpc_id = module.vpc.vpc_id
}

resource "aws_security_group_rule" "ingress_http_alb_public" {
  security_group_id = aws_security_group.alb_private.id
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  type              = "ingress"
}

resource "aws_security_group_rule" "ingress_https_alb_public" {
  security_group_id = aws_security_group.alb_private.id
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  type              = "ingress"
}

