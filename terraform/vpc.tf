locals {
  # subnet tags are used for aws-alb-ingress-controller
  # https://github.com/kubernetes-sigs/aws-alb-ingress-controller/blob/master/docs/guide/controller/config.md#subnet-auto-discovery
  eks_public_subnets_tags = {
    "kubernetes.io/cluster/eks"      = "shared"
    "kubernetes.io/role/alb-ingress" = ""
    "kubernetes.io/role/elb"         = ""
  }

  eks_private_subnets_tags = {
    "kubernetes.io/cluster/eks"       = "shared"
    "kubernetes.io/role/alb-ingress"  = ""
    "kubernetes.io/role/internal-elb" = ""
  }
}

module "vpc" {
  source = "./modules/vpc/"

  vpc_cidr                        = var.vpc_cidr
  azs                             = var.availability_zones
  public_subnets                  = var.public_subnets
  private_subnets                 = var.private_subnets
  eks_workers_subnets             = var.eks_workers_subnets
  additional_public_subnets_tags  = local.eks_public_subnets_tags
  additional_private_subnets_tags = local.eks_private_subnets_tags
  eks_workers_subnets_tags        = local.eks_private_subnets_tags
  is_flow_logs_enabled            = false
}
