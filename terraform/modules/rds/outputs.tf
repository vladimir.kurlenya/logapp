output "name" {
  value       = join("", aws_rds_cluster.default.*.database_name)
  description = "Database name"
}

output "user" {
  value       = join("", aws_rds_cluster.default.*.master_username)
  description = "Username for the master DB user"
}

output "password" {
  value       = join("", aws_rds_cluster.default.*.master_password)
  description = "Password for the master DB user"
  sensitive   = true
}

output "cluster_name" {
  value       = join("", aws_rds_cluster.default.*.cluster_identifier)
  description = "Cluster Identifier"
}

output "endpoint" {
  value       = join("", aws_rds_cluster.default.*.endpoint)
  description = "Cluster primary endpoint"
}

output "port" {
  value       = join("", aws_rds_cluster.default.*.port)
  description = "Cluster Endpoint port"
}

output "reader_endpoint" {
  value       = join("", aws_rds_cluster.default.*.reader_endpoint)
  description = "Cluster reader endpoint"
}

output "cluster_arn" {
  value       = join("", aws_rds_cluster.default.*.arn)
  description = "Cluster ARN"
}
