variable "name" {
  type        = string
  description = "Name of the cluster"
}

variable "salt" {
  type        = string
  default     = ""
  description = "Additional string for db name"
}

variable "security_groups" {
  type        = list(string)
  description = "List of security groups to be allowed to connect to the DB instance"
}

variable "vpc_id" {
  type        = string
  description = "VPC ID to create the cluster in (e.g. `vpc-a22222ee`)"
}

variable "subnets" {
  type        = list(string)
  description = "List of VPC subnet IDs"
}

# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/rds_cluster#availability_zones
variable "availability_zones" {
  type        = list(string)
  description = "List of Availability Zones that instances in the DB cluster can be created in"
}

variable "instance_type" {
  type        = string
  default     = "db.t2.small"
  description = "Instance type to use"
}

variable "apply_immediately" {
  type        = string
  default     = true
  description = "Flag to instruct the service to apply the change immediately "
}

variable "cluster_size" {
  type        = string
  default     = "2"
  description = "Number of DB instances to create in the cluster"
}

variable "snapshot_identifier" {
  type        = string
  default     = ""
  description = "Specifies whether or not to create this cluster from a snapshot"
}

variable "name_ssm_override" {
  type        = string
  default     = ""
  description = "helpful when you cluster name contains workspace var and you want to make you aws ssm secrets consistent (the same name in all envs)"
}

variable "db_name" {
  type        = string
  description = "Database name"
}

variable "db_port" {
  type        = string
  default     = "3306"
  description = "Database port"
}

variable "admin_user" {
  type        = string
  default     = "admin"
  description = "(Required unless a snapshot_identifier is provided) Username for the master DB user"
}

variable "admin_password" {
  type        = string
  description = "(Required unless a snapshot_identifier is provided) Password for the master DB user"
}

variable "retention_period" {
  type        = string
  default     = "7"
  description = "Number of days to retain backups for"
}

variable "backup_window" {
  type        = string
  default     = "07:00-09:00"
  description = "Daily time range during which the backups happen"
}

variable "maintenance_window" {
  type        = string
  default     = "wed:03:00-wed:04:00"
  description = "Weekly time range during which system maintenance can occur, in UTC"
}

variable "delimiter" {
  type        = string
  default     = "-"
  description = "Delimiter to be used between `name`, `namespace`, `stage` and `attributes`"
}

variable "attributes" {
  type        = list(string)
  default     = []
  description = "Additional attributes (e.g. `policy` or `role`)"
}

variable "tags" {
  type        = map(string)
  description = "Additional tags (e.g. map(`BusinessUnit`,`XYZ`)"
}

# https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-rds-database-instance.html#cfn-rds-dbinstance-allowmajorversionupgrade
variable "allow_major_version_upgrade" {
  default     = true
  description = "allow major engine version upgrades when changing engine versions"
}

# explanation why we need name prefix
# https://github.com/hashicorp/terraform-provider-aws/issues/6448#issuecomment-848389991
# https://github.com/hashicorp/terraform-provider-aws/issues/17357#issuecomment-819370097
variable "name_prefix_for_parameter_group_is_used" {
  default = true
}

variable "cluster_parameters" {
  type = list(any)

  # default is for engine "aurora-postgresql", please override this var for other engine
  default     = []
  description = "List of DB parameters to apply"
}

variable "cluster_family" {
  type        = string
  default     = "aurora5.6"
  description = "The family of the DB cluster parameter group"
}

variable "engine" {
  type        = string
  default     = "aurora"
  description = "The name of the database engine to be used for this DB cluster. Valid values: `aurora`, `aurora-postgresql`"
}

variable "engine_version" {
  type        = string
  default     = ""
  description = "The version number of the database engine to use"
}

variable "allowed_cidr_blocks" {
  type        = list(string)
  default     = []
  description = "List of CIDR blocks allowed to access"
}

variable "enabled" {
  description = "Set to false to prevent the module from creating any resources"
  default     = true
}

variable "replication_source_identifier" {
  description = "ARN of the master cluster"
  type        = string
  default     = ""
}

variable "publicly_accessible" {
  description = "Boolean determining if RDS cluster is publicly accessible"
  default     = false
}

variable "additional_main_database_roles" {
  type        = list(string)
  default     = []
  description = "List of additional roles allowed to connect to the DB instance"
}

variable "additional_databases" {
  type        = list(string)
  default     = []
  description = "List of additional databases and roles allowed to create in DB instance. Served with ansible only."
}

variable "deletion_protection" {
  default = true
}

variable "storage_encrypted" {
  default     = true
  description = "If set to true, the underlying storage will be encrypted with default KMS key"
}

variable "kms_key_id" {
  default     = ""
  description = "KMS key id"
}

variable "monitoring_interval" {
  default     = "60"
  description = "The interval, in seconds, between points when Enhanced Monitoring metrics are collected for the DB instance"
}

variable "performance_insights_enabled" {
  default = true
}

variable "generate_performance_insights_kms_key" {
  default = true
}

variable "iam_auth_enabled" {
  default = true
}

variable "logs_type_to_cloudwatch" {
  default = []
}

variable "ca_cert_identifier" {
  description = "RDS SSL ca type"
  default     = "rds-ca-2019"
}
