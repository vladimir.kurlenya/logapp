locals {
  aws_ssm_prefix = var.name_ssm_override == "" ? "/rds_cluster/${var.name}" : "/rds_cluster/${var.name_ssm_override}"
}

resource "aws_rds_cluster" "default" {
  count                               = var.enabled ? 1 : 0
  allow_major_version_upgrade         = var.allow_major_version_upgrade
  cluster_identifier                  = var.name
  database_name                       = var.db_name
  master_username                     = var.admin_user
  master_password                     = var.admin_password
  backup_retention_period             = var.retention_period
  preferred_backup_window             = var.backup_window
  final_snapshot_identifier           = lower(var.name)
  copy_tags_to_snapshot               = true
  skip_final_snapshot                 = true
  apply_immediately                   = true
  vpc_security_group_ids              = var.security_groups
  preferred_maintenance_window        = var.maintenance_window
  db_subnet_group_name                = aws_db_subnet_group.default[0].name
  availability_zones                  = var.availability_zones
  enabled_cloudwatch_logs_exports     = var.logs_type_to_cloudwatch
  iam_database_authentication_enabled = var.iam_auth_enabled

  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.default[0].name

  tags = merge(
    var.tags,
    {
      Name   = var.name
      Backup = "true"
    },
  )
  engine              = var.engine
  engine_version      = var.engine_version
  deletion_protection = var.deletion_protection
  storage_encrypted   = var.storage_encrypted
  kms_key_id          = var.kms_key_id

  snapshot_identifier = var.snapshot_identifier

  # - "replication_source_identifier" works only during creation cluster from sratch
  # - we added it to "ignore_changes" as changing value back to "" not Promoted read replica to Write (you need promote it manually)
  replication_source_identifier = var.replication_source_identifier

  lifecycle {
    ignore_changes = [
      replication_source_identifier,
      snapshot_identifier,
      availability_zones,
    ]
  }
}

resource "aws_rds_cluster_instance" "default" {
  count                           = var.enabled ? var.cluster_size : 0
  identifier                      = "${var.name}-${var.salt}${count.index + 1}"
  cluster_identifier              = aws_rds_cluster.default[0].id
  instance_class                  = var.instance_type
  apply_immediately               = var.apply_immediately
  db_subnet_group_name            = aws_db_subnet_group.default[0].name
  publicly_accessible             = var.publicly_accessible
  engine                          = var.engine
  engine_version                  = var.engine_version
  auto_minor_version_upgrade      = false
  monitoring_interval             = var.monitoring_interval
  monitoring_role_arn             = aws_iam_role.rds_enhanced_monitoring[0].arn
  performance_insights_enabled    = var.performance_insights_enabled
  performance_insights_kms_key_id = var.generate_performance_insights_kms_key ? aws_kms_key.performance_insights[0].arn : ""
  ca_cert_identifier              = var.ca_cert_identifier

  tags = merge(
    var.tags,
    {
      Backup = "true"
    },
  )
}

data "aws_iam_policy_document" "rds_enhanced_monitoring" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["monitoring.rds.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "rds_enhanced_monitoring" {
  count              = var.enabled ? 1 : 0
  name               = "monitoring-${var.name}"
  assume_role_policy = data.aws_iam_policy_document.rds_enhanced_monitoring.json
}

resource "aws_iam_role_policy_attachment" "rds_enhanced_monitoring" {
  count      = var.enabled ? 1 : 0
  role       = aws_iam_role.rds_enhanced_monitoring[0].name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
}

resource "aws_kms_key" "performance_insights" {
  count               = var.enabled ? 1 : 0
  description         = "KMS key to encrypt Performance Insights data for ${var.name}"
  enable_key_rotation = false
  tags = merge(
    var.tags,
    {
      "Name" = var.name
    },
  )
}

resource "aws_kms_alias" "performance_insights" {
  count         = var.enabled ? 1 : 0
  name          = "alias/${var.name}-performance-insights"
  target_key_id = aws_kms_key.performance_insights[0].key_id
}

resource "aws_db_subnet_group" "default" {
  count       = var.enabled ? 1 : 0
  name        = var.name
  description = "Allowed subnets for DB cluster instances"
  subnet_ids  = var.subnets
  tags        = var.tags
}

resource "aws_rds_cluster_parameter_group" "default" {
  count       = var.enabled ? 1 : 0
  name        = var.name_prefix_for_parameter_group_is_used ? null : var.name
  name_prefix = var.name_prefix_for_parameter_group_is_used ? "${var.name}-" : null
  description = "DB cluster parameter group"
  family      = var.cluster_family
  dynamic "parameter" {
    for_each = var.cluster_parameters
    content {
      apply_method = parameter.value.apply_method
      name         = parameter.value.name
      value        = parameter.value.value
    }
  }
  tags = merge(
    var.tags,
    {
      "Name" = var.name
    },
  )

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_ssm_parameter" "cluster_identifier" {
  count = var.enabled ? 1 : 0
  name  = "${local.aws_ssm_prefix}/cluster_identifier"
  type  = "String"
  value = aws_rds_cluster.default[0].cluster_identifier
  tags  = var.tags
}

resource "aws_ssm_parameter" "host" {
  count = var.enabled ? 1 : 0
  name  = "${local.aws_ssm_prefix}/host"
  type  = "String"
  value = aws_rds_cluster.default[0].endpoint
  tags  = var.tags
}

resource "aws_ssm_parameter" "reader_host" {
  count = var.enabled ? 1 : 0
  name  = "${local.aws_ssm_prefix}/reader_host"
  type  = "String"
  value = aws_rds_cluster.default[0].reader_endpoint
  tags  = var.tags
}

resource "aws_ssm_parameter" "database_name" {
  count = var.enabled ? 1 : 0
  name  = "${local.aws_ssm_prefix}/database_name"
  type  = "String"
  value = aws_rds_cluster.default[0].database_name
  tags  = var.tags
}

resource "aws_ssm_parameter" "master_username" {
  count = var.enabled ? 1 : 0
  name  = "${local.aws_ssm_prefix}/master_username"
  type  = "String"
  value = aws_rds_cluster.default[0].master_username
  tags  = var.tags
}

resource "aws_ssm_parameter" "master_password" {
  count = var.enabled ? 1 : 0
  name  = "${local.aws_ssm_prefix}/master_password"
  type  = "SecureString"
  value = aws_rds_cluster.default[0].master_password
  tags  = var.tags
}
