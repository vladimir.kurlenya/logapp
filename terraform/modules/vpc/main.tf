resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags                 = var.additional_vpc_tags
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = var.additional_vpc_tags
}

# Subnets

resource "aws_subnet" "private_subnet" {
  count             = length(var.private_subnets)
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = element(var.private_subnets, count.index)
  availability_zone = element(var.azs, count.index)
  tags = merge(
    var.additional_private_subnets_tags,
    {
      Name = "private-${element(var.azs, count.index)}"
    },
  )
}

resource "aws_subnet" "public_subnet" {
  count             = length(var.public_subnets)
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = element(var.public_subnets, count.index)
  availability_zone = element(var.azs, count.index)

  tags = merge(
    var.additional_public_subnets_tags,
    {
      Name = "public-${element(var.azs, count.index)}"
    },
  )
}

resource "aws_subnet" "eks_workers_subnet" {
  count             = length(var.eks_workers_subnets)
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = element(var.eks_workers_subnets, count.index)
  availability_zone = element(var.azs, count.index)

  tags = merge(
    var.eks_workers_subnets_tags,
    {
      Name = "private-${element(var.azs, count.index)}"
    },
  )
}

#NAT gateway

resource "aws_eip" "nat_eip" {
  count = length(var.public_subnets)
  vpc   = true

  tags = {
    Name = "nat-eip-${element(var.azs, count.index)}"
  }
}

resource "aws_nat_gateway" "nat_gw" {
  count         = length(var.public_subnets)
  allocation_id = element(aws_eip.nat_eip.*.id, count.index)
  subnet_id     = element(aws_subnet.public_subnet.*.id, count.index)
  depends_on = [
    aws_internet_gateway.igw,
    aws_eip.nat_eip,
  ]

  tags = {
    Name = element(var.azs, count.index)
  }
}

# Routing tables and routes

resource "aws_route_table" "private" {
  count  = length(var.private_subnets)
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "private-${element(var.azs, count.index)}"
  }

  depends_on = [aws_nat_gateway.nat_gw]
}

resource "aws_route" "private_nat_gateway" {
  count                  = length(var.private_subnets)
  route_table_id         = element(aws_route_table.private.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = element(aws_nat_gateway.nat_gw.*.id, count.index)
}

resource "aws_route_table" "eks_workers" {
  count  = length(var.eks_workers_subnets)
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "eks-workers-${element(var.azs, count.index)}"
  }

  depends_on = [aws_nat_gateway.nat_gw]
}

resource "aws_route" "eks_workers_nat_gateway" {
  count                  = length(var.eks_workers_subnets)
  route_table_id         = element(aws_route_table.eks_workers.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = element(aws_nat_gateway.nat_gw.*.id, count.index)
}

resource "aws_route" "public_internet_gateway" {
  count = length(var.public_subnets) > 0 ? 1 : 0

  route_table_id         = aws_route_table.public[0].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id
}

resource "aws_route_table" "public" {
  count  = length(var.public_subnets) > 0 ? 1 : 0
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "public-${element(var.azs, count.index)}"
  }

  depends_on = [aws_internet_gateway.igw]
}

#Route tables association

resource "aws_route_table_association" "private" {
  count          = length(var.private_subnets)
  subnet_id      = element(aws_subnet.private_subnet.*.id, count.index)
  route_table_id = element(aws_route_table.private.*.id, count.index)
}

resource "aws_route_table_association" "eks_workers" {
  count          = length(var.eks_workers_subnets)
  subnet_id      = element(aws_subnet.eks_workers_subnet.*.id, count.index)
  route_table_id = element(aws_route_table.eks_workers.*.id, count.index)
}

resource "aws_route_table_association" "public" {
  count          = length(var.public_subnets)
  subnet_id      = element(aws_subnet.public_subnet.*.id, count.index)
  route_table_id = aws_route_table.public[0].id
}

resource "aws_flow_log" "vpc" {
  count                = var.is_flow_logs_enabled ? 1 : 0
  log_destination_type = "s3"
  log_destination      = var.flow_log_bucket_arn
  traffic_type         = "ACCEPT"
  vpc_id               = aws_vpc.vpc.id

}
