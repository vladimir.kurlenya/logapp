output "private_subnets_ids" {
  value = aws_subnet.private_subnet.*.id
}

output "eks_workers_subnets_ids" {
  value = aws_subnet.eks_workers_subnet.*.id
}

output "public_subnets_ids" {
  value = aws_subnet.public_subnet.*.id
}

output "vpc_id" {
  value = join("", aws_vpc.vpc.*.id)
}

output "vpc_cidr_block" {
  value = join("", aws_vpc.vpc.*.cidr_block)
}

output "public_route_tables_ids" {
  value = aws_route_table.public.*.id
}

output "private_route_tables_ids" {
  value = aws_route_table.private.*.id
}

output "eks_workers_route_tables_ids" {
  value = aws_route_table.eks_workers.*.id
}

output "nat_gateway_public_ips" {
  value = aws_nat_gateway.nat_gw.*.public_ip
}
