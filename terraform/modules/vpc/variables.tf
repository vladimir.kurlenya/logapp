variable "vpc_cidr" {
  type    = string
  default = ""
}

variable "private_subnets" {
  type    = list(string)
  default = []
}

variable "public_subnets" {
  type    = list(string)
  default = []
}

variable "eks_workers_subnets" {
  type    = list(string)
  default = []
}

variable "azs" {
  type    = list(string)
  default = []
}

variable "additional_vpc_tags" {
  type        = map(string)
  description = "Additional tags for vpc"
  default     = {}
}

variable "additional_public_subnets_tags" {
  type        = map(string)
  description = "Additional tags for public subnets"
  default     = {}
}

variable "additional_private_subnets_tags" {
  type        = map(string)
  description = "Additional tags for private subnets"
  default     = {}
}

variable "eks_workers_subnets_tags" {
  type        = map(string)
  description = "Additional tags for eks_workers subnets"
  default     = {}
}

variable "flow_log_bucket_arn" {
  default = ""
}

variable "flow_log_default_vpc_bucket_arn" {
  default = ""
}

variable "is_flow_logs_enabled" {
  default = "0"
}
