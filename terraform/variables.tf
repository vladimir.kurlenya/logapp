variable "aws_access_key" {
  default = ""
}
variable "aws_secret_key" {
  default = ""
}

variable "availability_zones" {
  default = [
    "us-east-1a",
    "us-east-1b",
    "us-east-1c"
  ]
}

variable "vpc_cidr" {
  default = "10.10.0.0/16"
}

variable "public_subnets" {
  default = [
    "10.10.128.0/24",
    "10.10.129.0/24",
    "10.10.130.0/24"
  ]
}

variable "private_subnets" {
  default = [
    "10.10.1.0/24",
    "10.10.2.0/24",
    "10.10.3.0/24"
  ]
}

variable "eks_workers_subnets" {
  default = [
    "10.10.16.0/20",
    "10.10.32.0/20",
    "10.10.48.0/20"
  ]
}

variable "eks_cluster_version" {
  default = "1.22"
}

variable "logapp_tag" {
}
