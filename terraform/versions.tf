terraform {
  required_version = ">= 0.14"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.72.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.10.0"
    }
    # null = {
    #   source  = "hashicorp/null"
    #   version = "2.1.2"
    # }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.1.0"
    }
    template = {
      source  = "hashicorp/template"
      version = "2.1.2"
    }
    # tls = {
    #   source  = "hashicorp/tls"
    #   version = "~> 3.1.0"
    # }
  }
}
