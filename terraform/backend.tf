provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = "us-east-1"
}

terraform {
  # backend "local" {
  #   path = "../terraform.tfstate"
  # }
  backend "s3" {
    bucket = "logap-tf-state"
    key    = "logapp.tfstate"
  }
}
