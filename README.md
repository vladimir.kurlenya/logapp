# logapp

Setup AWS environmnet and deploy test application named "logapp""

## Summary

The application uses PostgreSQL as a datastore and expects the URL and the credentials to be passed in the DATABASE_URL environment variable. The service itself has 2 endpoints (in app.py), one for the service itself and the other one for the healthcheck.

### What haven't been done yet

  * Application is not covered with pytest

  * Terraform is not covered with terratest

  * Helm is not covered with helm-unittest

  * EKS autoscaling is not implemented ([CA](https://github.com/kubernetes/autoscaler/tree/master/cluster-autoscaler) or [karpenter](https://karpenter.sh/) can be used)

  * Helm test hook is implemented but not used while helm deployment is in terraform (see no quick way except local-exec to reach this)

  * Application flask app run in dev mode. Uwsgi can be used.

  * Terraform apply running is not locked (dynamodb or gitlab deployments feature can be used)

  * Application use master db password. Need to switch to IAM. This requires run bastion, because db is not accessible outside vpc (we need grant rds_iam)

### Tools and environment

  * We will deliver application infrastructure to AWS.

  * We will use AWS EKS as a platform for application. We've got maintainability, observability, high availability anf fault tolerance with this decision.

  * We will use dedicated VPC for EKS.

  * EKS control plane will run in "private" subnets.

  * EKS pyload will run in dedidated "private" subnets (eks_workers_subnets).

  * We will use ALB to expose our application out of VPC.

  * ALB will be placed into public subnets and will be able to answer over 80 port. We will not serve https in this example.

  * ALB allows to communicate with EKS workers nodeports.

  * We will use terraform for infrastructure delivery.

  * Several community terraform modules will be used ([terraform-aws-eks](github.com/terraform-aws-modules/terraform-aws-eks) and [irsa](terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks)). Just to reduce implement time consuming.

  * We we deploy application using Helm but via terraform too.

  * We will store docker images in ecr

  * Ecr crepo creation and docker image build are moved out of terraform

  * We will use [aws-load-balancer-controller](https://kubernetes-sigs.github.io/aws-load-balancer-controller) to automate exposing application 

  * We will use gitlab ci as a deployment runtime  

### Deployment

  * ./deploy.sh can be used for a local testing. We assume the terraform (tested on 14 and 15), aws-cli-v2, python and others are installed. Env variables are exported too.
  
  * .gitlab-ci.yml can be used to deploy over gitlab

  * Both ways expect env vars
    - AWS_ACCESS_KEY_ID
    - AWS_DEFAULT_REGION
    - AWS_SECRET_ACCESS_KEY
    - TF_VAR_aws_access_key
    - TF_VAR_aws_secret_key

